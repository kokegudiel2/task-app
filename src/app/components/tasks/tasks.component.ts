import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  tasksList: Task[] = [];
  taskName: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  addTask(): void {
    const task: Task = {
      name: this.taskName,
      state: false
    }

    this.tasksList.push(task);

    this.taskName = '';
  }

  deleteTask(index: number): void {
    console.info(index);
    this.tasksList.splice(index, 1);
  }

  updateTask(task: Task, index: number): void {
    this.tasksList[index].state = !task.state;
  }
}
